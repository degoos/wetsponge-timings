import Vue from 'vue'

const App = () => import('./App.vue');
const Main = () => import( './components/Main');
const Paste = () => import( './components/Paste');

import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

Vue.use(VueRouter);
Vue.use(VueResource);

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'mdbvue/build/css/mdb.css';

import {Time, TimeWriter} from "timecount";

const timeWriter = new TimeWriter({decimalPlaces: 0, verbose: false});

import * as vClickOutside from 'v-click-outside-x';

Vue.use(vClickOutside);

Vue.config.productionTip = false;

const router = new VueRouter({
    routes: [
        // dynamic segments start with a colon
        {path: '/', component: Main},
        {path: '/paste', component: Paste},
        {path: '/paste/:id', component: Paste}
    ]
});

Vue.filter('nanoToMs', function (num) {
    return timeWriter.countdown(Time.from(num, "nanosecond"), ["minute", "second", "milisecond"]);
});

Vue.filter('prettyBytes', function (num) {
    // jacked from: https://github.com/sindresorhus/pretty-bytes
    if (typeof num !== 'number' || isNaN(num)) {
        throw new TypeError('Expected a number');
    }

    var exponent;
    var unit;
    var neg = num < 0;
    var units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    if (neg) {
        num = -num;
    }

    if (num < 1) {
        return (neg ? '-' : '') + num + ' B';
    }

    exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1);
    num = (num / Math.pow(1000, exponent)).toFixed(2) * 1;
    unit = units[exponent];

    return (neg ? '-' : '') + num + ' ' + unit;
});

new Vue({
    render: h => h(App),
    router,
}).$mount('#app');
